![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![internaluse-green](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# RATIONALE #

* Good practices on metadata insertion on digital assets, according to guidelines and checklists by Scopus
![scopus.jpeg](https://bitbucket.org/repo/64jkj76/images/1207730341-scopus.jpeg)

### What is this repository for? ###

* Quick summary
    - Actions, checklists and procedures applied on xml files, therefore over on metadata datum: gathering data, formatting, internal testings, sorting, _et alia_
	- A systematize-to_do-checklist formula to generate `.xml` files format according [Scopus indexing procedure](https://www.ubijournal.com/scopus-indexed-journals/)
* Version 1.03

### How do I get set up? ###

* Summary of set up
    - _Vide_ [Colophon.md](https://bitbucket.org/imhicihu/scopus-metadata/src/e20bd6d0b69dacf52ea480137fbb338396d44844/Colophon.md?at=master&fileviewer=file-view-default)
* Configuration
    - Check [Colophon.md](https://bitbucket.org/imhicihu/scopus-metadata/src/master/Colophon.md)
* Dependencies
    - Up to now, there is no dependencies
* Database configuration
    - `.xml` file format treatment and handling needs some [software](https://bitbucket.org/imhicihu/scopus-metadata/src/master/Colophon.md) to apply the recipe
* How to run tests
    - Check [data gathered.md](https://bitbucket.org/imhicihu/scopus-metadata/src/master/Data_gathered.md)
* Deployment instructions
    - Check [XML formatting.md](https://bitbucket.org/imhicihu/scopus-metadata/src/master/XML_formatting.md)
    - Check [Bibliography.md](https://bitbucket.org/imhicihu/scopus-metadata/src/master/Bibliography.md)

### Related repositories ###

* Some repositories linked with this project:
     - [Temas Medievales](https://bitbucket.org/imhicihu/temas-medievales-project/src/)
     - [PDF-Inner structure](https://bitbucket.org/imhicihu/pdf-inner-structure/src/master/)

### Issues ###

* Check them on [here](https://bitbucket.org/imhicihu/scopus-metadata/issues)

### Changelog ###

* Please check the [Commits](https://bitbucket.org/imhicihu/scopus-metadata/commits/) section for the current status

### Who do I talk to? ###

* Repo owner or admin
    - Contact `imhicihu` at `gmail` dot `com`
* Other community or team contact
    - Contact is _enable_ on the [board](https://bitbucket.org/imhicihu/scopus-metadata/addon/trello/trello-board) of this repo. (You need a [Trello](https://trello.com/) account)

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/scopus-metadata/src/master/code_of_conduct.md)

### Legal ###

* All trademarks are the property of their respective owners.

### Licence ###

* The content of this project itself is licensed under the ![MIT Licence](https://bitbucket.org/repo/ekyaeEE/images/2049852260-MIT-license-green.png)   